const Vaccines = [
    {
        id: "1",
        VaccineName: "Hepatitis",
        Doses: "3"
    },

    {
        id: "2",
        VaccineName: "BCG",
        Doses: "1"
    },

    {
        id: "3",
        VaccineName: "DTPa/DTPw",
        Doses: "3"
    },

    {
        id: "4",
        VaccineName: "Boosters",
        Doses: "2"
    },

    {
        id: "5",
        VaccineName: "Poliomyletis",
        Doses: "3"
    }
]

export default Vaccines;