import React from 'react'
import DashForm from "./DashForm.css";
import { NavLink } from "react-router-dom";

function DashBoard() {
  return (
    <div className='bbody'>
      <nav>
        <div className='logo'>Babyshots
          <ul>
            <li><NavLink to="">Home</NavLink></li>
            <li><NavLink to="schedule">Schedule</NavLink></li>
            <li><NavLink to="tracker">Tracker</NavLink></li>
            <li><NavLink to="register">Register</NavLink></li>
            <li><NavLink to="login">Login</NavLink></li>
          </ul>
        </div>
      </nav>
    </div> 
  )
}

export default DashBoard;