import React from 'react';
import './RegisForm.css';

function RegisterForm() {
  return (
    <div className='main2'>
    <div className='sub-main2'>
    <div>
      <div className='imgs2'>
        <div className="container-image2">
          <img src="images/BabyshotsLogo.png" alt='' className='BabyshotsLogo2'/>
        
  
        </div>


      </div>
    <div>
        <h1>Register Page</h1>
        <br />
        <div>
        <label>Username: </label><br />
        <input type="text" placeholder="Username" className="Username" />
        </div>
    </div>
      
    <div>
        <label>Password: </label><br />
        <input type="password" placeholder="Password" className="Password" />
    </div>
    <div>
        <label>Child's Name: </label><br />
        <input type="text" placeholder="LastName, FirstName" className="Username" />
    </div>
    <div>
        <label>Address: </label><br />
        <input type="text" placeholder="address" className="Username" />
    </div>
    <div>
        <label>Birthdate: </label><br />
        <input type="date" className="Username" />
    </div>
    <div>
        <label>Email: </label><br />
        <input type="text" placeholder="example@gmail.com" className="Username" />
    </div>
    <div>
        <label>Phone Number: </label><br />
        <input type="text" placeholder="09*******" className="Username" />
    </div>
    <div>
        <label>Insurance Company: </label><br />
        <input type="text" placeholder="Company name" className="Username" />
    </div>
    <div>
        <label>Insurance ID: </label><br />
        <input type="text" placeholder="Company ID" className="Username" />
    </div>

        <div className="login-button">
        <br /><button>Register Here</button>
        </div>
      </div>
    </div>
    </div>
  )
}

export default RegisterForm;

