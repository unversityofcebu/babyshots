import React, { Fragment } from 'react';
import {Table, Form} from 'react-bootstrap';
import "bootstrap/dist/css/bootstrap.min.css";
import Vaccines from './Vaccines';
import './VaccineTracker.css';




 function VaccineTracker() {
  return (
    <Fragment> 
      <div style={{margin:"10rem"}}>
        <div style={{marginBottom:"5rem"}}>
          <h1>
            Vaccine Tracker
          </h1>
        </div>
        <Table border hover size='sm'>
           <thead>
            <tr>
              <th>
                Vaccine
              </th>
              <th>
                Doses
              </th>
              <th>
                Date
              </th>
              <th>
                Site
              </th>
              <th>
                Doctor
              </th>
              <th>
                Vaccination Status
              </th>
            </tr>
           </thead>
           <tbody>
              {
                /*ROWS*/
                Vaccines && Vaccines.length > 0
                ?
                Vaccines.map((item) =>{
                  return(
                    <tr>
                      <td>
                        {item.VaccineName}
                      </td>
                      <td>
                          <Form>
                              {['checkbox'].map((type) => (
                                <div key={`default-${type}`} style={{marginBottom:45, marginTop: 15}}>
                                  <Form.Check 
                                    type={type}
                                    id={``}
                                    label={`1st Dose`}
                                  />
                                </div>
                              ))}
                            </Form> 
                            <Form>
                              {['checkbox'].map((type) => (
                                <div key={`default-${type}`} style={{marginBottom:45, marginTop: 15}}>
                                  <Form.Check 
                                    type={type}
                                    id={``}
                                    label={`2nd Dose`}
                                  />
                                </div>
                              ))}
                            </Form> 
                            <Form>
                              {['checkbox'].map((type) => (
                                <div key={`default-${type}`} style={{marginBottom:45, marginTop: 15}}>
                                  <Form.Check 
                                    type={type}
                                    id={``}
                                    label={`3rd Dose`}
                                  />
                                </div>
                              ))}
                            </Form> 
                            
                            
                      </td>
                      <td>
                            <form>
                              <Form.Control
                              type="datetime-local"
                              placeholder="mm/dd/yyyy"
                              aria-label=""
                              
                              />
                            <br/>
                            <Form.Control
                              type="datetime-local"
                              placeholder="mm/dd/yyyy"
                              aria-label=""
                              
                              />
                            <br/>
                            <Form.Control
                              type="datetime-local"
                              placeholder="mm/dd/yyyy"
                              aria-label=""
                              
                              />
                            </form>
                          </td>
                          <td>
                          <form>
                              <Form.Control
                              type="textarea"
                              placeholder="Address"
                              aria-label=""
                             
                              />
                            <br/>
                            <Form.Control
                              type="textarea"
                              placeholder="Address"
                              aria-label=""
                             
                              />
                            <br/>
                            <Form.Control
                              type="textarea"
                              placeholder="Address"
                              aria-label=""
                              
                              />
                            </form>
                      </td>
                      <td>
                          <form>
                              <Form.Control
                              type="text"
                              placeholder="MD. John Doe"
                              aria-label=""
                             
                              />
                            <br/>
                            <Form.Control
                              type="text"
                              placeholder="MD. John Doe"
                              aria-label=""
                              
                              />
                            <br/>
                            <Form.Control
                              type="text"
                              placeholder="MD. John Doe"
                              aria-label=""
                             
                              />
                            </form>
                      </td>
                      <td>
                          <Form>
                              {['checkbox'].map((type) => (
                                <div key={`default-${type}`} style={{marginBottom:45, marginTop: 15}}>
                                  <Form.Check 
                                    type={type}
                                    id={``}
                                    label={``}
                                  />
                                </div>
                              ))}
                            </Form> 
                            <Form>
                              {['checkbox'].map((type) => (
                                <div key={`default-${type}`} style={{marginBottom:45, marginTop: 15}}>
                                  <Form.Check 
                                    type={type}
                                    id={``}
                                    label={``}
                                  />
                                </div>
                              ))}
                            </Form> 
                            <Form>
                              {['checkbox'].map((type) => (
                                <div key={`default-${type}`} style={{marginBottom:45, marginTop: 15}}>
                                  <Form.Check 
                                    type={type}
                                    id={``}
                                    label={``}
                                  />
                                </div>
                              ))}
                            </Form> 
                      </td>
                    </tr>
                  )
                })
                :
                "No data"
                  
                
              }
           </tbody>
        </Table>
      </div>
    </Fragment>
  )
}

export default VaccineTracker;

