import React from 'react';
import "./SetSched.css"

function SetSchedule() {
  return (
    <div className='main3'>
    <div className='sub-main3'>
    <div>
      <div className='imgs3'>
        <div className="container-image3">
          <img src="images/BabyshotsLogo.png" alt='' className='BabyshotsLogo3'/>
        
  
        </div>


      </div>
    <div>
        <h1>Set A Schedule</h1>
        <br />
        <div>
        <label>Patient's Name: </label><br /><br />
        <input type="text" placeholder="Username" className="Username" />
        </div>
    </div>
    <br />
    <div>
        <label>Vaccination: </label>
        <br />
        <br />
        <select name='vax' className='selection'>
            <option value="hepA">Hepatitis A</option>
            <option value="hepB">Hepatitis b</option>
            <option value="rota">Rotavirus</option>
            <option value="polio">Polio</option>
        </select>
    </div>
    <br />
    <div>
        <label>Date: </label>
        <br /><br />
        <input type="datetime-local" id="dates" />
    </div>
    
    <div className="login-button">
        <br /><button>Confirm</button>
    </div>
      </div>
    </div>
    </div>
  )
}

export default SetSchedule;

