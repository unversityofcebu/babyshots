import React from 'react'
import "./Login.css";



 function LoginForm() {
  return (
    <div className='main'>
      <div className='sub-main'>
      <div>
        <div className='imgs'>
          <div className="container-image">
            <img src="images/BabyshotsLogo.png" alt='' className='BabyshotsLogo'/>
          
    
          </div>


        </div>
        <div>
          <h1>Login Page</h1>
          <div>
          <input type="text" placeholder="Username" className="Username" />
          </div>
        </div>
        
        <div>
          <input type="password" placeholder="Password" className="Password" />
          </div>  
          <div className="login-button">
          <button>Login</button>
          </div>
          <div>
            <p>
              <a href="#">Forgot password?</a> or  <a href="RegisterForm.js">Sign Up</a>
            </p>
          </div>
        </div>
      </div>
      </div>
    
  )
}

export default LoginForm;