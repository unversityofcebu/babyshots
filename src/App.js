import './App.css';
import DashBoard from './components/DashBoard';
import LoginForm from './components/LoginForm';
import { BrowserRouter } from "react-router-dom";
import { Routes, Route } from "react-router-dom";
import RegisterForm from './components/RegisterForm';
import SetSchedule from './components/SetSchedule';
import VaccineTracker from './components/VaccinTracker';

function App() {  
  return (
    <div className="App">

      <BrowserRouter>
        <Routes>
          <Route path="/" element={<DashBoard />} />
          <Route path="/login" element={<LoginForm />} />
          <Route path="/register" element={<RegisterForm />} />
          <Route path="/schedule" element={<SetSchedule />} />
          <Route path="/tracker" element={<VaccineTracker />} />
        </Routes>
      </BrowserRouter>

    </div>
  );
}

export default App;